﻿
#include <iostream>
#include <string>

int main()
{
    std::string line = "What a wonderful world";
            
    std::cout << line << "\n";
    std::cout << line.length() << "\n";
    char first = 'W';
    char last = 'd';
    std::cout << "The first symbol in the line is " << first << "\n" << "The last symbol in the line is " << last;
}

